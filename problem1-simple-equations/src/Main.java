import java.util.*;

public class Main {
    public static void main(String[] args) {
        simpleEquation(6,6, 14);
        simpleEquation(1,2,3);
    }

    public static void simpleEquation(Integer A, Integer B, Integer C) {
        int max = Math.max(Math.max(A, B), C);

        for (int i = 0; i<=max; i++) {
            for (int j = 0; j<=max; j++) {
                for (int k = 0; k<=max; k++) {
                    if (count(i,j,k).equals(A) && multiplication(i,j,k).equals(B) && pow(i,j,k) == (double) C) {
                        System.out.printf("%d, %d, %d" , i, j ,k);
                        System.out.println();
                        return;
                    }
                }
            }
        }
        System.out.println("No Solution");
    }

    public static Integer count(Integer x, Integer y, Integer z) {
        return x+y+z;
    }

    public static Integer multiplication(Integer x, Integer y, Integer z) {
        return x*y*z;
    }

    public static Double pow(Integer x, Integer y, Integer z) {
        return Math.pow((double) x, 2) + Math.pow((double) y, 2) + Math.pow((double) z, 2);
    }
}